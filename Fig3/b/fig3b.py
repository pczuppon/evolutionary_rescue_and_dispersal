import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))

################################
################################ Theory
################################

def phi2(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = snew + snew * (1-f+pim*f)*(snew-sold)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (snew-sold)*pim*f)/(np.sqrt(nenner))
    return(result)

def phi1(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = sold + sold * (1-f+pim*f)*(sold-snew)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (sold-snew)*(1-f))/(np.sqrt(nenner))
    return(result)

def phi1_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp((1-m1)*(1+sold)*(p1-1) + m1*(1+snew)*(p2-1))
    return(result)

def phi2_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp(m2*(1+sold)*(p1-1) + (1-m2)*(1+snew)*(p2-1))
    return(result)

def pop2(f,piw,mu,v_w,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    result = min(Knew,mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu))))
    return(result)

def proba(f,piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew):
    Phi = 1-np.exp(-tfin*u*D*(phi1(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew)*f*Kold + phi2(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew)*(1-f)*pop2(f,piw,mu,v_w,Kold,Knew)))
    return(Phi)

def proba_n(f,piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,v1,v2):
    Phi = 1-np.exp(-tfin*u*D*(v1*f*Kold + v2*(1-f)*pop2(f,piw,mu,v_w,Kold,Knew)))
    return(Phi)

############### GLOBAL PARAMETER
w_m = 1.35
w_w = 1.5
v_w = 0.75
v_m = 1.02
mu = 0.06
f = np.arange(0.01,0.95,0.001)
Kold = 1000
Knew = 500
D = 10
tfin = 100
u = 1/(Knew*D)
   
################################
################################ pi = 1
################################
my_data = genfromtxt('vary_f_mut_pi1_0_pi2_0.txt', delimiter=',')

pihat = 0.
piw = np.exp(pihat)
pim = np.exp(pihat)

data1 = []
freq1 = []
for i in range(len(my_data)):
    data1.append(float(my_data[i][0]))
    freq1.append(float(my_data[i][1]))

freq1 = np.asarray(freq1)

resc_ev1 = []
for i in range(len(f)):
    resc_ev1.append(max(0,proba(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew)))

phi1_n1 = np.zeros(len(f))
phi2_n1 = np.zeros(len(f))
for i in range(len(f)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n1[i]
        phi2_n_old = phi2_n1[i]
        phi1_n1[i] = phi1_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n1[i] = phi2_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

resc_n1 = []
for i in range(len(f)):
    resc_n1.append(max(0,proba_n(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,1-phi1_n1[i],1-phi2_n1[i])))

################################
################################ pi =05
################################
my_data = genfromtxt('vary_f_mut_pi1_m05_pi2_m05.txt', delimiter=',')

pihat = -0.5
piw = np.exp(pihat)
pim = np.exp(pihat)

data2 = []
freq2 = []
for i in range(len(my_data)):
    data2.append(float(my_data[i][0]))
    freq2.append(float(my_data[i][1]))

freq2 = np.asarray(freq2)

resc_ev2 = []
for i in range(len(f)):
    resc_ev2.append(max(0,proba(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew)))

phi1_n2 = np.zeros(len(f))
phi2_n2 = np.zeros(len(f))
for i in range(len(f)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n2[i]
        phi2_n_old = phi2_n2[i]
        phi1_n2[i] = phi1_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n2[i] = phi2_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

resc_n2 = []
for i in range(len(f)):
    resc_n2.append(max(0,proba_n(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,1-phi1_n2[i],1-phi2_n2[i])))


################################
################################ pi = 2
################################
my_data = genfromtxt('vary_f_mut_pi1_05_pi2_05.txt', delimiter=',')

pihat = 0.5
piw = np.exp(pihat)
pim = np.exp(pihat) 

data3 = []
freq3 = []
for i in range(len(my_data)):
    data3.append(float(my_data[i][0]))
    freq3.append(float(my_data[i][1]))

freq3 = np.asarray(freq3)

resc_ev3 = []
for i in range(len(f)):
    resc_ev3.append(max(0,proba(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew)))

phi1_n3 = np.zeros(len(f))
phi2_n3 = np.zeros(len(f))
for i in range(len(f)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n3[i]
        phi2_n_old = phi2_n3[i]
        phi1_n3[i] = phi1_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n3[i] = phi2_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

resc_n3 = []
for i in range(len(f)):
    resc_n3.append(max(0,proba_n(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,1-phi1_n3[i],1-phi2_n3[i])))

################################
################################ pi1=2, pi2 =0.5
################################
my_data = genfromtxt('vary_f_mut_pi1_05_pi2_m05.txt', delimiter=',')

pi1hat = 0.5
pi2hat = -0.5
piw = np.exp(pi1hat)
pim = np.exp(pi2hat)

data4 = []
freq4 = []
for i in range(len(my_data)):
    data4.append(float(my_data[i][0]))
    freq4.append(float(my_data[i][1]))

freq4 = np.asarray(freq4)

resc_ev4 = []
for i in range(len(f)):
    resc_ev4.append(max(0,proba(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew)))

phi1_n4 = np.zeros(len(f))
phi2_n4 = np.zeros(len(f))
for i in range(len(f)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n4[i]
        phi2_n_old = phi2_n4[i]
        phi1_n4[i] = phi1_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n4[i] = phi2_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

resc_n4 = []
for i in range(len(f)):
    resc_n4.append(max(0,proba_n(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,1-phi1_n4[i],1-phi2_n4[i])))

################################
################################ pi1=0.5, pi2 =2
################################
my_data = genfromtxt('vary_f_mut_pi1_m05_pi2_05.txt', delimiter=',')

pi1hat = -0.5
pi2hat = 0.5
piw = np.exp(pi1hat)
pim = np.exp(pi2hat)

data5 = []
freq5 = []
for i in range(len(my_data)):
    data5.append(float(my_data[i][0]))
    freq5.append(float(my_data[i][1]))

freq5 = np.asarray(freq5)

resc_ev5 = []
for i in range(len(f)):
    resc_ev5.append(max(0,proba(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew)))

phi1_n5 = np.zeros(len(f))
phi2_n5 = np.zeros(len(f))
for i in range(len(f)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n5[i]
        phi2_n_old = phi2_n5[i]
        phi1_n5[i] = phi1_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n5[i] = phi2_num(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

resc_n5 = []
for i in range(len(f)):
    resc_n5.append(max(0,proba_n(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,1-phi1_n5[i],1-phi2_n5[i])))

################################
################################ Plot
################################
plt.plot(f,resc_ev1,linewidth=2,color='black',linestyle='--')
plt.plot(f,resc_n1,linewidth=2,color='black')
plt.plot(freq1/10,data1,'o',color='black',markersize=10)

plt.plot(f,resc_ev2,linewidth=2,color='green',linestyle='--')
plt.plot(f,resc_n2,linewidth=2,color='green')
plt.plot(freq2/10,data2,'gv',markersize=10)

plt.plot(f,resc_ev3,linewidth=2,color='blue',linestyle='--')
plt.plot(f,resc_n3,linewidth=2,color='blue')
plt.plot(freq3/10,data3,'b^',markersize=10)

plt.plot(f,resc_ev4,linewidth=2,color='orange',linestyle='dashed')
plt.plot(f,resc_n4,linewidth=2,color='orange')
plt.plot(freq4/10,data4,'s',color='orange',markersize=10)

plt.plot(f,resc_ev5,linewidth=2,color='purple',linestyle='dashed')
plt.plot(f,resc_n5,linewidth=2,color='purple')
plt.plot(freq5/10,data5,'D',color='purple',markersize=10)

#plt.xlim((10**(-3),1))
plt.ylim((-0.05,0.65))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10) 
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5) 

plt.show()


