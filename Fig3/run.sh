#!/bin/bash

g++ -std=c++11 sim_vary_m_mut.cpp `pkg-config --libs gsl`

for i in 0.001 0.0015 0.002 0.003 0.004 0.005 0.006 0.008 0.01 0.015 0.02 0.03 0.04 0.05 0.06 0.08 0.1 0.15 0.2 0.3 0.4 0.5 0.6 0.8 1.
do
    qsub script.sh "$i"
done
