#include <iostream>
#include <numeric>
#include <algorithm>
#include <random>
#include <fstream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

// g++ -std=c++11 sim_vary_f_mut.cpp `pkg-config --libs gsl` command for compiling

using namespace std;

#define K1 1000       // Carrying capacity old habitat
#define K2 500      // Carrying capacity new habitat
#define demes 10    // number of patches
#define mu 0.06   // number of old habitat patches
#define sel 0.02    // selection coefficient in the new habitat (mutant) 
#define surv 0.75   // survival rate of the wild type in the new habitat
#define pi1 -0.5     // dispersal bias towards old habitat (wild type)
#define pi2 -0.5     // dispersal bias towards new habitat (mutant)
#define w_m 1.35      // fecundity fitness of the mutant in the old habitat
#define w_w 1.5    // fecundity fitness of the wild type in the old habitat
#define repeats 100000  // number of repetitions of the stochastic simulations
#define tfin 100        // studied time-frame
#define u 1/((double)K2*(double)demes)   // mutation rate


int success, r_ind;     // auxiliary variables
double avg;                 
int result;


// Random number generation with Mersenne Twister
gsl_rng * r = gsl_rng_alloc (gsl_rng_mt19937);

int RUN(int);

int main(int argc,char *argv[])
{
    int e1_number = atoi(argv[1]);      // dispersal rate (external input)
    success = 0;
       
    for (r_ind=0; r_ind<repeats; r_ind++)
    {
        gsl_rng_set(r,r_ind);           // setting the seed
        if (RUN(e1_number)>0) success += 1;    // stochastic simulation
    }
    avg = ((double)success)/((double)repeats);  // averaging the establishment probability

    ofstream file ("vary_f_mut_pi1_m05_pi2_m05.txt", ios::app);   // file output
    file << avg;
    file << ", ";
    file << e1_number; 
    file << "\n";
    file.close();
    
    return(0);
}

int RUN(int e1_number)      // stochastic simulation
{
    int mt[demes], wt[demes], mt_work[demes], wt_work[demes], generation = 0, ind, mutants;   // auxiliary variables
    
    // transformed dispersal bias
    double pi1_hat, pi2_hat;        
    
    pi1_hat = exp(pi1);
    pi2_hat = exp(pi2);
        
    // Initialization of the system
    for (ind=0; ind<demes; ind++)
    {
        mt[ind] = 0;    // no mutants
        wt[ind] = K1;    // wild type at carrying capcity
    }
    
    // old patch frequency
    double f = (double)e1_number/(double)demes;
    
    // Initialize wild type in new habitats in stationary state        
    for (ind=e1_number; ind < demes; ind++)
    {
        wt[ind] = min((double)K2, round(f* (double)K1 * mu *surv / (mu*f*pi1_hat + (1.-surv)*(1.-f+f*pi1_hat - mu*f*pi1_hat))));
        if (wt[ind]<0) wt[ind]=0;
    }

    // Stochastic simulation in discrete time
    while(generation < tfin || accumulate(mt,mt+sizeof(mt)/sizeof(mt[0]),0) > 0)
    {
        // Initialize working arrays
        copy(mt, mt+demes, mt_work);
        copy(wt, wt+demes, wt_work);

        // Migration - defining the number of migrants
        unsigned int gsl_ran_binomial(const gsl_rng * r, double p, unsigned int n);
        
        int migrants[demes], migrants1[demes];
        
        for (ind=0; ind<demes; ind++)
        {
            migrants[ind] = gsl_ran_binomial(r,mu,wt_work[ind]);
            migrants1[ind] = gsl_ran_binomial(r,mu,mt_work[ind]);
        }

        int migrantpool_wt = accumulate(migrants,migrants+demes,0);
        int migrantpool_mt = accumulate(migrants1,migrants1+demes,0);

        // Migration - number of migrants into old patches
        int mto1_wt = gsl_ran_binomial(r,pi1_hat*((double)e1_number)/(pi1_hat*((double)e1_number)+(double)demes - (double)e1_number),migrantpool_wt);
        int mto1_mt = gsl_ran_binomial(r,pi2_hat*((double)e1_number)/(pi2_hat*((double)e1_number)+(double)demes - (double)e1_number),migrantpool_mt);

        // Migration - distribution among patches (multinomial)
        int ssize;
        void gsl_ran_multinomial(const gsl_rng * r, size_t ssize, unsigned int N, const double p[], unsigned int n[]);

        double pro1[e1_number];
        
        for (int ind=0;ind<e1_number;ind++)
        {
            pro1[ind] = 1/(double)e1_number;
        }

        double pro2[demes-e1_number];
        
        for (int ind=0;ind<demes-e1_number;ind++)
        {
            pro2[ind] = 1/((double)demes-(double)e1_number);
        }     
     
        unsigned int immigrants01[e1_number], immigrants11[e1_number];
        unsigned int immigrants02[demes-e1_number], immigrants12[demes-e1_number];
        
        gsl_ran_multinomial(r,e1_number,mto1_wt,pro1,immigrants01);
        gsl_ran_multinomial(r,e1_number,mto1_mt,pro1,immigrants11);
        gsl_ran_multinomial(r,demes-e1_number,migrantpool_wt-mto1_wt,pro2,immigrants02);
        gsl_ran_multinomial(r,demes-e1_number,migrantpool_mt-mto1_mt,pro2,immigrants12);

        // Updating the numbers after migration
        for (int ind=0; ind < demes; ind++)
        {
            if (ind<e1_number)
            {
                wt_work[ind] = wt_work[ind] - migrants[ind] + immigrants01[ind];
                mt_work[ind] = mt_work[ind] - migrants1[ind] + immigrants11[ind];
            }
            else
            {
                wt_work[ind] = wt_work[ind] - migrants[ind] + immigrants02[ind-e1_number];
                mt_work[ind] = mt_work[ind] - migrants1[ind] + immigrants12[ind-e1_number];
            }
        }

        // Reproduction, Mutation (until tfin) and Regulation
        unsigned int gsl_ran_poisson(const gsl_rng * r, double lambda);
        unsigned int gsl_ran_hypergeometric(const gsl_rng * r, unsigned int n1, unsigned int n2, unsigned int t);
                
        // Old patches: Poisson offspring number + regulation (hypergeometric) if necessary
        for (int i=0; i<e1_number; i++)
        {   
            wt[i] = gsl_ran_poisson(r,(double)wt_work[i]*w_w);
            mt[i] = gsl_ran_poisson(r,(double)mt_work[i]*w_m);
            
            // Mutation until generation tfin
            if (generation < tfin)
            {
                mutants = gsl_ran_binomial(r,u,wt[i]);
                mt[i] += mutants;
                wt[i] -= mutants;
                
            }
           
            // Regulation
            if (mt[i]+wt[i]>K1)
            {
                wt[i] = gsl_ran_hypergeometric(r,wt[i],mt[i],K1);
                mt[i] = K1-wt[i];
            }
        }

        // New patches: Poisson offspring number + regulation (hypergeometric) if necessary
        for (int i=e1_number; i<demes; i++)
        {
            mt[i] = gsl_ran_poisson(r,(double)mt_work[i]*(1+sel));
            wt[i] = gsl_ran_poisson(r,(double)wt_work[i]*surv);
            
            // Mutations until generation tfin (otherwise skip this step)
            if (generation < tfin)
            {
                mutants = gsl_ran_binomial(r,u,wt[i]);
                wt[i] -= mutants;
                mt[i] += mutants;
            }
            
            // Regulation
            if (mt[i]+wt[i]>K2)
            {
                wt[i] = gsl_ran_hypergeometric(r,wt[i],mt[i],K2);
                mt[i] = K2-wt[i];
            }
        }

        // Terminating condition: if mutant number larger than 60% of carrying capacity in new or old patches
        if (accumulate(mt+e1_number, mt+demes, 0) >= 0.6*(double)K2*5 || accumulate(mt,mt+e1_number,0) >= 0.6*(double)K1*5) break;

        generation++;
    }
    result = accumulate(mt,mt+sizeof(mt)/sizeof(mt[0]),0);
    return(result);
}
