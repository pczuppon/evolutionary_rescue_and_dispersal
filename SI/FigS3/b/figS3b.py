import numpy as np
import matplotlib.pyplot as plt

############### Parameter definitions
w_m = 1.45		# fecundities in old habitat
w_w = 1.5
v_w = 0.75		# fecundities in new habitat
v_m = 1.02
Kold = 1000		# carrying capacities
Knew = 500
D = 10
f = 0.5			# old-habitat frequencies

pi1 = np.exp(0)
pi2 = np.exp(0.5)
pi3 = np.exp(-0.5)

m = np.arange(0.0001,1.0001,0.0001)


############## Compute popsizes after dispersal
N_null = []
N_pos = []
N_neg = []
for i in range(len(m)):
	N_null.append(m[i]*(1-f)/(1-f+pi1*f)*f/(1-f) *Kold + (1-m[i]+m[i]*(1-f)/(1-f+pi1*f)) * m[i]*v_w*f*Kold/(1-f+pi1*f-v_w*(1-f+pi1*f*(1-m[i]))))
	N_pos.append(m[i]*(1-f)/(1-f+pi3*f)*f/(1-f) *Kold + (1-m[i]+m[i]*(1-f)/(1-f+pi3*f)) * m[i]*v_w*f*Kold/(1-f+pi3*f-v_w*(1-f+pi3*f*(1-m[i]))))
	N_neg.append(m[i]*(1-f)/(1-f+pi2*f)*f/(1-f) *Kold + (1-m[i]+m[i]*(1-f)/(1-f+pi2*f)) * m[i]*v_w*f*Kold/(1-f+pi2*f-v_w*(1-f+pi2*f*(1-m[i]))))


############## compute local growth rates
a_null = []
a_pos = []
a_neg = []
for i in range(len(m)):
	if (N_null[i] >= Knew/v_w):
		a_null.append(Knew*v_m/(v_w*N_null[i])-1)
	else:
		a_null.append(v_m-1)
	
	if (N_pos[i] >= Knew/v_w):
		a_pos.append(Knew*v_m/(v_w*N_pos[i])-1)
	else:
		a_pos.append(v_m-1)
	
	if (N_neg[i] >= Knew/v_w):
		a_neg.append(Knew*v_m/(v_w*N_neg[i])-1)
	else:
		a_neg.append(v_m-1)

############## plot
plt.semilogx(m,a_null,linewidth=2,color='black')
plt.plot(m,a_pos,linewidth=2,color='green')
plt.plot(m,a_neg,linewidth=2,color='blue')

plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.xlim((0.001,1))
#plt.ylim((0,500))
plt.show()

