import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))


################################
################################ Data
################################

my_data = genfromtxt('vary_mu_rescue_origin_pi1_m05_pi2_m05.txt', delimiter=',')

prob1 = []
prob2 = []
prob3 = []
mu_data = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1.append(float(my_data[i][0])/tot)
    prob2.append(float(my_data[i][1])/tot)
    prob3.append(float(my_data[i][2])/tot)
    mu_data.append(float(my_data[i][3]))

my_data = genfromtxt('vary_mu_rescue_origin_pi1_05_pi2_05.txt', delimiter=',')

prob1s = []
prob2s = []
prob3s = []
mu_datas = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1s.append(float(my_data[i][0])/tot)
    prob2s.append(float(my_data[i][1])/tot)
    prob3s.append(float(my_data[i][2])/tot)
    mu_datas.append(float(my_data[i][3]))

my_data = genfromtxt('vary_mu_rescue_origin_pi1_05_pi2_m05.txt', delimiter=',')

prob1g = []
prob2g = []
prob3g = []
mu_datag = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1g.append(float(my_data[i][0])/tot)
    prob2g.append(float(my_data[i][1])/tot)
    prob3g.append(float(my_data[i][2])/tot)
    mu_datag.append(float(my_data[i][3]))

my_data = genfromtxt('vary_mu_rescue_origin_pi1_m05_pi2_05.txt', delimiter=',')

prob1f = []
prob2f = []
prob3f = []
mu_dataf = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1f.append(float(my_data[i][0])/tot)
    prob2f.append(float(my_data[i][1])/tot)
    prob3f.append(float(my_data[i][2])/tot)
    mu_dataf.append(float(my_data[i][3]))

my_data = genfromtxt('vary_mu_rescue_origin_pi1_0_pi2_0.txt', delimiter=',')

prob1a = []
prob2a = []
prob3a = []
mu_dataa = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1a.append(float(my_data[i][0])/tot)
    prob2a.append(float(my_data[i][1])/tot)
    prob3a.append(float(my_data[i][2])/tot)
    mu_dataa.append(float(my_data[i][3]))


################################
################################ plot
################################
plt.semilogx(mu_dataa,prob1a,'x',color='black',ms=10,mew=2)
plt.plot(mu_dataa,prob2a,'+',color='black',ms=10,mew=2)
plt.plot(mu_dataa,prob3a,'o',fillstyle='none',color='black',ms=10,mew=2)
plt.semilogx(mu_data,prob1,'x',color='green',ms=10,mew=2)
plt.plot(mu_data,prob2,'+',color='green',ms=10,mew=2)
plt.plot(mu_data,prob3,'o',fillstyle='none',color='green',ms=10,mew=2)
plt.semilogx(mu_datas,prob1s,'x',color='blue',ms=10,mew=2)
plt.plot(mu_datas,prob2s,'+',color='blue',ms=10,mew=2)
plt.plot(mu_datas,prob3s,'o',fillstyle='none',color='blue',ms=10,mew=2)
plt.semilogx(mu_datag,prob1g,'x',color='orange',ms=10,mew=2)
plt.plot(mu_datag,prob2g,'+',color='orange',ms=10,mew=2)
plt.plot(mu_datag,prob3g,'o',fillstyle='none',color='orange',ms=10,mew=2)
plt.semilogx(mu_dataf,prob1f,'x',color='purple',ms=10,mew=2)
plt.plot(mu_dataf,prob2f,'+',color='purple',ms=10,mew=2)
plt.plot(mu_dataf,prob3f,'o',fillstyle='none',color='purple',ms=10,mew=2)

plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.xlim((10**(-3),1))
plt.show()

