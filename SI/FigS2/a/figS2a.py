import numpy as np
import matplotlib.pyplot as plt

############### Parameter definitions
w_m = 1.45
w_w = 1.5
v_w = 0.75
v_m = 1.02
Kold = 1000
Knew = 500
D = 10
f = 0.5

pi1 = np.exp(0)
pi2 = np.exp(0.5)
pi3 = np.exp(-0.5)

m = np.arange(0.0001,1.0001,0.0001)

############## Compute popsizes after dispersal
N_null = []
N_pos = []
N_neg = []
for i in range(len(m)):
	N_null.append((1-m[i]+m[i]*pi1*f/(1-f+pi1*f))*Kold + m[i]*pi1*f/(1-f+pi1*f)*(1-f)/f * m[i]*v_w*f*Kold/(1-f+pi1*f-v_w*(1-f+pi1*f*(1-m[i]))))
	N_pos.append((1-m[i]+m[i]*pi3*f/(1-f+pi3*f))*Kold + m[i]*pi3*f/(1-f+pi3*f)*(1-f)/f * m[i]*v_w*f*Kold/(1-f+pi3*f-v_w*(1-f+pi3*f*(1-m[i]))))
	N_neg.append((1-m[i]+m[i]*pi2*f/(1-f+pi2*f))*Kold + m[i]*pi2*f/(1-f+pi2*f)*(1-f)/f * m[i]*v_w*f*Kold/(1-f+pi2*f-v_w*(1-f+pi2*f*(1-m[i]))))


############## plot
plt.semilogx(m,N_null,linewidth=2,color='black')
plt.plot(m,N_pos,linewidth=2,color='green')
plt.plot(m,N_neg,linewidth=2,color='blue')

plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.xlim((0.001,1))
plt.ylim((0,1250))
plt.show()

