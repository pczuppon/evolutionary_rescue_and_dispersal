#include <iostream>
#include <numeric>
#include <algorithm>
#include <random>
#include <fstream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

// g++ -std=c++11 sim_vary_mu.cpp `pkg-config --libs gsl` command for compiling

using namespace std;

#define K 500       // Carrying capacity
#define demes 10    // number of patches
#define e1_number 5   // number of old habitat patches
#define sel 0.02    // selection coefficient in the new habitat (mutant) 
#define surv 0.75   // survival rate of the wild type in the new habitat
#define pi1 2.0     // dispersal bias towards old habitat (wild type)
#define pi2 0.5     // dispersal bias towards new habitat (mutant)
#define w_m 9.      // fecundity fitness of the mutant in the old habitat
#define w_w 10.0    // fecundity fitness of the wild type in the old habitat
#define beta 0.0    // competition strength in the new habitat
#define repeats 100000  // number of repetitions of the stochastic simulations


int success, r_ind;     // auxiliary variables
double avg;                 
int result;

// Random number generation with Mersenne Twister
gsl_rng * r = gsl_rng_alloc (gsl_rng_mt19937);

int RUN(double,int);

int main(int argc,char *argv[])
{
    double mu = atof(argv[1]);      // dispersal rate (external input)
    int scenario = atoi(argv[2]);   // distinction between mutant startin in old patches (=0) or new patches (=1)
    success = 0;
       
    for (r_ind=0; r_ind<repeats; r_ind++)
    {
        gsl_rng_set(r,r_ind);           // setting the seed
        if (RUN(mu,scenario)>0) success += 1;    // stochastic simulation
    }
    avg = ((double)success)/((double)repeats);  // averaging the establishment probability

    ofstream file ("vary_mu_phi2_pi1_2_pi2_05.txt", ios::app);   // file output
    file << avg;
    file << ", ";
    file << mu; 
    file << "\n";
    file.close();
    
    return(0);
}

int RUN(double mu,int scenario)      // stochastic simulation
{
    int mt[demes], wt[demes], mt_work[demes], wt_work[demes], generation = 0, ind;   // auxiliary variables
        
    // Initialization of the system
    for (ind=0; ind<demes; ind++)
    {
        mt[ind] = 0;    // no mutants
        wt[ind] = K;    // wild type at carrying capcity
    }
    
    // old patch frequency
    double f = (double)e1_number/(double)demes;
    
    // Initialize wild type in new habitats in stationary state        
    for (ind=e1_number; ind < demes; ind++)
    {
        wt[ind] = min((double)K, round(f* (double)K * mu *surv* (1.-f) / ((1.-f+pi1*f) * (1.-surv + surv*mu*(1-(1-f)/(1-f+pi1*f))))));
    }
    
    if (scenario==0)    // introduce one mutant in old habitat
    {
        mt[0] = 1;
        wt[0] -=1;
    }
    
    if (scenario==1)        // introduce one mutant in new habitat
    {
        mt[e1_number] = 1;      
        wt[e1_number] -= 1;
    }

    if (wt[e1_number]<0) wt[e1_number]=0;
    
    // Stochastic simulation in discrete time
    while(accumulate(mt,mt+sizeof(mt)/sizeof(mt[0]),0) > 0 )
    {
        // Initialize working arrays
        copy(mt, mt+demes, mt_work);
        copy(wt, wt+demes, wt_work);

        // Migration - defining the number of migrants
        unsigned int gsl_ran_binomial(const gsl_rng * r, double p, unsigned int n);
        
        int migrants[demes], migrants1[demes];
        
        for (ind=0; ind<demes; ind++)
        {
            migrants[ind] = gsl_ran_binomial(r,mu,wt_work[ind]);
            migrants1[ind] = gsl_ran_binomial(r,mu,mt_work[ind]);
        }

        int migrantpool_wt = accumulate(migrants,migrants+demes,0);
        int migrantpool_mt = accumulate(migrants1,migrants1+demes,0);

        // Migration - number of migrants into old patches
        int mto1_wt = gsl_ran_binomial(r,pi1*((double)e1_number)/(pi1*((double)e1_number)+(double)demes - (double)e1_number),migrantpool_wt);
        int mto1_mt = gsl_ran_binomial(r,pi2*((double)e1_number)/(pi2*((double)e1_number)+(double)demes - (double)e1_number),migrantpool_mt);

        // Migration - distribution among patches (multinomial)
        int ssize;
        void gsl_ran_multinomial(const gsl_rng * r, size_t ssize, unsigned int N, const double p[], unsigned int n[]);

        double pro1[e1_number];
        
        for (int ind=0;ind<e1_number;ind++)
        {
            pro1[ind] = 1/(double)e1_number;
        }

        double pro2[demes-e1_number];
        
        for (int ind=0;ind<demes-e1_number;ind++)
        {
            pro2[ind] = 1/((double)demes-(double)e1_number);
        }     
     
        unsigned int immigrants01[e1_number], immigrants11[e1_number];
        unsigned int immigrants02[demes-e1_number], immigrants12[demes-e1_number];
        
        gsl_ran_multinomial(r,e1_number,mto1_wt,pro1,immigrants01);
        gsl_ran_multinomial(r,e1_number,mto1_mt,pro1,immigrants11);
        gsl_ran_multinomial(r,demes-e1_number,migrantpool_wt-mto1_wt,pro2,immigrants02);
        gsl_ran_multinomial(r,demes-e1_number,migrantpool_mt-mto1_mt,pro2,immigrants12);

        // Updating the numbers after migration
        for (int ind=0; ind < demes; ind++)
        {
            if (ind<e1_number)
            {
                wt_work[ind] = wt_work[ind] - migrants[ind] + immigrants01[ind];
                mt_work[ind] = mt_work[ind] - migrants1[ind] + immigrants11[ind];
            }
            else
            {
                wt_work[ind] = wt_work[ind] - migrants[ind] + immigrants02[ind-e1_number];
                mt_work[ind] = mt_work[ind] - migrants1[ind] + immigrants12[ind-e1_number];
            }
        }
    
        // Reproduction
        // Old patches: Wright-Fisher sampling (binomial)
        for (int i=0; i<e1_number; i++)
        {   
            mt[i] = gsl_ran_binomial(r,w_m*(double)mt_work[i]/(w_m*(double)mt_work[i]+w_w*(double)wt_work[i]),K);
            wt[i] = K-mt[i];
        }

        // New patches: Poisson offspring number + regulation (hypergeometric) if necessary
        unsigned int gsl_ran_poisson(const gsl_rng * r, double lambda);
        unsigned int gsl_ran_hypergeometric(const gsl_rng * r, unsigned int n1, unsigned int n2, unsigned int t);
        
        for (int i=e1_number; i<demes; i++)
        {
            mt[i] = gsl_ran_poisson(r,(double)mt_work[i]*(1+sel*(1-beta*((double)wt_work[i]+(double)mt_work[i])/(double)K)));
            wt[i] = gsl_ran_poisson(r,(double)wt_work[i]*surv);
            if (mt[i]+wt[i]>K)
            {
                wt[i] = gsl_ran_hypergeometric(r,wt[i],mt[i],K);
                mt[i] = K-wt[i];
            }
        }

        // Terminating condition: if mutant number larger than 80% of carrying capacity in new or old patches
        if (accumulate(mt+e1_number, mt+demes, 0) >= 0.8*(double)K*5 || accumulate(mt,mt+e1_number,0) >= 0.8*(double)K*5) break;

        generation++;
    }
    result = accumulate(mt,mt+sizeof(mt)/sizeof(mt[0]),0);
    return(result);
}
