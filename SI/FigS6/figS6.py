import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))


################################
################################ tau = 100
################################

my_data = genfromtxt('rescue_sgv_tau.txt', delimiter=',')

d1_sgv = []
d1_dn = []
d1_both = []
tau_data1 = []
for i in range(len(my_data)):
    d1_sgv.append(float(my_data[i][0]))
    d1_dn.append(float(my_data[i][1]))
    d1_both.append(float(my_data[i][2]))
    tau_data1.append(float(my_data[i][3]))


################################
################################ plot
################################
plt.semilogx(tau_data1, d1_sgv,'x',color='black',markersize = 10,mew=2)
plt.plot(tau_data1,d1_dn,'+',color='black',markersize = 10,mew=2)
plt.plot(tau_data1,d1_both,'o',color='black',markersize = 10,mew=2)

#plt.xlim((0.001,1))
plt.ylim((0,0.3))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.show()

