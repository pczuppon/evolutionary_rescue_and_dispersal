import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))

################################
################################ Theory
################################

def phi2(f,piw,pim,mu,w_m,w_w,r,s,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    if (m1*(1-r)*f*Kold/((1-f)*(1-(1-m2)*(1-r))) < Knew):
        sold = w_m/(w_w*(1-r*mu*(1-f)/(f*mu*piw+r*(1-f+piw*f*(1-mu)))))-1
        #if (mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))) < Knew):
        snew = s
        #else:
        #    snew = Knew * (1+s)/(mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))))-1
    else:
        sold = w_m*Kold/(w_w *((1-f)*mu*piw*Knew + Kold*(1-mu-f*(1-mu-piw)))/(1-f+piw*f) )-1
        #if ((1-r)*(Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f) < Knew):
        snew = s
        #else:
        #    snew = Knew*(1+s)/((Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f))-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = snew + snew * (1-f+pim*f)*(snew-sold)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (snew-sold)*pim*f)/(np.sqrt(nenner))
    return(result)

def phi1(f,piw,pim,mu,w_m,w_w,r,s,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    if (m1*(1-r)*f*Kold/((1-f)*(1-(1-m2)*(1-r))) < Knew):
        sold = w_m/(w_w*(1-r*mu*(1-f)/(f*mu*piw+r*(1-f+piw*f*(1-mu)))))-1
        #if (mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))) < Knew):
        snew = s
        #else:
        #    snew = Knew * (1+s)/(mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))))-1
    else:
        sold = w_m*Kold/(w_w *((1-f)*mu*piw*Knew + Kold*(1-mu-f*(1-mu-piw)))/(1-f+piw*f) )-1
        #if ((1-r)*(Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/v(1-f+piw*f) < Knew):
        snew = s
        #else:
        #    snew = Knew*(1+s)/((Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f))-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = sold + sold * (1-f+pim*f)*(sold-snew)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (sold-snew)*(1-f))/(np.sqrt(nenner))
    return(result)

def phi1_num(f,piw,pim,mu,w_m,w_w,r,s,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    if (m1*(1-r)*f*Kold/((1-f)*(1-(1-m2)*(1-r))) < Knew):
        sold = w_m/(w_w*(1-r*mu*(1-f)/(f*mu*piw+r*(1-f+piw*f*(1-mu)))))-1
        #if (mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))) < Knew):
        snew = s
        #else:
        #    snew = Knew * (1+s)/(mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))))-1
    else:
        sold = w_m*Kold/(w_w *((1-f)*mu*piw*Knew + Kold*(1-mu-f*(1-mu-piw)))/(1-f+piw*f) )-1
        #if ((1-r)*(Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f) < Knew):
        snew = s
        #else:
        #    snew = Knew*(1+s)/((Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f))-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp((1-m1)*(1+sold)*(p1-1) + m1*(1+snew)*(p2-1))
    return(result)

def phi2_num(f,piw,pim,mu,w_m,w_w,r,s,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    if (m1*(1-r)*f*Kold/((1-f)*(1-(1-m2)*(1-r))) < Knew):
        sold = w_m/(w_w*(1-r*mu*(1-f)/(f*mu*piw+r*(1-f+piw*f*(1-mu)))))-1
        #if (mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))) < Knew):
        snew = s
        #else:
        #    snew = Knew * (1+s)/(mu*f*Kold*(1-r)/(mu*piw*f+r*(1-f+piw*f*(1-mu))))-1
    else:
        sold = w_m*Kold/(w_w *((1-f)*mu*piw*Knew + Kold*(1-mu-f*(1-mu-piw)))/(1-f+piw*f) )-1
        #if ((1-r)*(Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f) < Knew):
        snew = s
        #else:
        #    snew = Knew*(1+s)/((Knew*(1-f+f*piw*(1-mu)) + Kold * mu*f)/(1-f+piw*f))-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp(m2*(1+sold)*(p1-1) + (1-m2)*(1+snew)*(p2-1))
    return(result)

################################
################################ parameter definition
################################
s_tilde = 0.02
f = np.arange(0.1,1.,0.01)
w_m = 1.35
w_w = 1.5
r= 0.25
Kold = 1000
Knew = 500
mu = 0.06

################################
################################ pi = 1
################################
pihat = 0.
piw = np.exp(pihat)
pim = np.exp(pihat)

phi1_ev1 = []
for i in range(len(f)):
    phi1_ev1.append(np.maximum(phi1(f[i],piw,pim,mu,w_m,w_w,r,s_tilde,Kold,Knew),0))

my_data = genfromtxt('vary_f_phi1_pi1_0_pi2_0.txt', delimiter=',')

data1 = []
f_data1 = []
for i in range(len(my_data)):
    data1.append(float(my_data[i][0]))
    f_data1.append(float(my_data[i][1]))

phi2_ev1 = []
for i in range(len(f)):
    phi2_ev1.append(np.maximum(phi2(f[i],piw,pim,mu,w_m,w_w,r,s_tilde,Kold,Knew),0))

my_data = genfromtxt('vary_f_phi2_pi1_0_pi2_0.txt', delimiter=',')

data2 = []
f_data2 = []
for i in range(len(my_data)):
    data2.append(float(my_data[i][0]))
    f_data2.append(float(my_data[i][1]))

phi1_n1 = np.zeros(len(f))
phi2_n1 = np.zeros(len(f))
for i in range(len(f)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n1[i]
        phi2_n_old = phi2_n1[i]
        phi1_n1[i] = phi1_num(f[i],piw,pim,mu,w_m,w_w,r,s_tilde,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n1[i] = phi2_num(f[i],piw,pim,mu,w_m,w_w,r,s_tilde,phi1_n_old,phi2_n_old,Kold,Knew)


f_data1 = np.asarray(f_data1)/10
f_data2 = np.asarray(f_data2)/10

################################
################################ Plot
################################
#plt.loglog(mu,np.maximum(phi1_ev1,0),linewidth=2,linestyle='dashed',color='black')
plt.plot(f,np.maximum(phi1_ev1,0),linewidth=2,linestyle='dashed',color='black')
plt.plot(f_data1, data1,'o',color='black',markersize=10)
plt.plot(f, 1-phi1_n1,linewidth=2,color = 'black')
 
plt.plot(f,np.maximum(phi2_ev1,0),linewidth=2,linestyle='dotted',color='black')
plt.plot(f,1-phi2_n1,linewidth=2,color='black',linestyle='-.')
plt.plot(f_data2,data2,'o',markersize=10,fillstyle='none',color='black')

#plt.ylim((-0.005, 0.05))
#plt.xlim((0.01,1))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.show()



