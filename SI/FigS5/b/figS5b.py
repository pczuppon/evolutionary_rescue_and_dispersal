import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))

################################
################################ Theory
################################

def pop2(f,piw,mu,r,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    if (m1*(1-r)*f*Kold/((1-f)*(1-(1-m2)*(1-r))) < Knew):
        result = Kold*mu*(1-r)*f/(mu*f*piw+r*(1-f+piw*f-mu*piw*f))
    else:
        result = Knew
    return(result)

################################
################################ parameter definition
################################
f = np.arange(0.1,1.,0.01)
r= 0.25
Kold = 1000
Knew = 500
mu = 0.06

################################
################################ pi = 1
################################
pihat = 0.
piw = np.exp(pihat)
pim = np.exp(pihat)

pop = []
for i in range(len(f)):
    pop.append(pop2(f[i],piw,mu,r,Kold,Knew))

pop = np.asarray(pop)

################################
################################ Plot
################################
#plt.loglog(mu,np.maximum(phi1_ev1,0),linewidth=2,linestyle='dashed',color='black')
plt.plot(f,f*Kold,linewidth=2,linestyle='dashed',color='black')
plt.plot(f, (1-f)*pop,linewidth=2,color = 'black')
 

#plt.ylim((-0.005, 0.05))
#plt.xlim((0.01,1))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.show()



