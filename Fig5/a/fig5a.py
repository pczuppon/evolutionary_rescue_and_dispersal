import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))

################################
################################ Theory - approximation
################################
def phi2(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = snew + snew * (1-f+pim*f)*(snew-sold)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (snew-sold)*pim*f)/(np.sqrt(nenner))
    return(result)

def phi1(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = sold + sold * (1-f+pim*f)*(sold-snew)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (sold-snew)*(1-f))/(np.sqrt(nenner))
    return(result)

def pop2_dev(mu,Kold,Knew,piw,v_w,D,tau):
    pop2_current = np.array([0]*D)
    t=0
    k=1
    m1 = mu * k/D /(k/D +piw*(1-k/D))
    pop2_current[0] = Kold
    work = pop2_current.copy()
    while (t < (D-1)*tau):
        if (t % tau == 0 and t>0):
            pop2_current[-1][k] = Kold
            work[k] = Kold
            k += 1
            m1 = mu * k/D /(k/D +piw*(1-k/D))
        summ = np.sum(work)
        for j in range(k):
            work[j] = np.minimum(work[j]*v_w*(1-mu) + m1*Kold*v_w*(D-k)/k + m1*v_w*summ/k,Knew)
        pop2_current = np.vstack([pop2_current,work])
        t+=1
    return(pop2_current)


def rescue(piw,pim,mu,w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew):
    pop2 = pop2_dev(mu,Kold,Knew,piw,v_w,D,tau)    # Population of wild-type individuals in new habitat patches
    resc_vector = []
    resc_vector2 = []
    
    for i in range(D-1):
        i+=1
        resc_vector.append(max(0,phi1(1-i/D,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew))*(D-i)*Kold*tau)   # Contribution from old patches
        
        t = (i-1)*tau
        while (t< i*tau):
            resc_vector2.append(max(0,phi2(1-i/D,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew))*np.sum(pop2[t])) # contribution from new patches
            t += 1    
    
    Phi = 1-np.exp(-u*np.sum(resc_vector)-u*np.sum(resc_vector2)-u*2*(v_m-1)*(Kold+np.sum(pop2[-1]))/(1-v_w))
    return(Phi)

################################
################################ Theory - exact
################################

def phi1_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp((1-m1)*(1+sold)*(p1-1) + m1*(1+snew)*(p2-1))
    return(result)

def phi2_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp(m2*(1+sold)*(p1-1) + (1-m2)*(1+snew)*(p2-1))
    return(result)

def rescue_num(piw,pim,mu,w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew,v1,v2):
    pop2 = pop2_dev(mu,Kold,Knew,piw,v_w,D,tau)
    resc_vector = []
    resc_vector2 = []
    
    for i in range(D-1):
        i+=1
        resc_vector.append(v1[8-i+1]*(D-i)*tau*Kold)
        
        t = (i-1)*tau
        while (t< i*tau):
            resc_vector2.append(v2[8-i+1]*np.sum(pop2[t]))
            t += 1
    
    result = 1-np.exp(-u*np.sum(resc_vector)-u*np.sum(resc_vector2)-u*2*(v_m-1)*(Kold+np.sum(pop2[-1]))/(1-v_w))
    return(result)


################################
################################ Parameter definitions
################################
w_m = 1.45
w_w = 1.5
v_w = 0.75
v_m = 1.02
D = 10
Kold = 1000
Knew = 500
u = 1/(25*Knew*D)
tau = 100

mu_plot = np.concatenate((np.arange(0.0005,0.001,0.0001),np.arange(0.001,0.01,0.0001),np.arange(0.01,0.1,0.001),np.arange(0.1,1.01,0.01)))

################################
################################ pi = 1
################################
pihat = 0.
piw = np.exp(pihat)
pim = np.exp(pihat)

res_ev1 = []
res_ev_n1 = []
#test = []
#f_plot = np.arange(0.1,1.1,0.1)
for i in range(len(mu_plot)):
    #test.append(phi2(f_plot[i],piw,pim,0.06,w_m,w_w,v_w,v_m,Kold,Knew))
    phi1_n1 = np.zeros(D-1)
    phi2_n1 = np.zeros(D-1)
    for j in range(D-1):
        k = 0
        for k in range(5000):
            phi1_n_old = phi1_n1[j]
            phi2_n_old = phi2_n1[j]
            phi1_n1[j] = phi1_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
            phi2_n1[j] = phi2_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
    
    res_ev1.append(rescue(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew))
    res_ev_n1.append(rescue_num(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew,1-phi1_n1,1-phi2_n1))

my_data = genfromtxt('rescue_vary_mu_pi1_0_pi2_0.txt', delimiter=',')

data1 = []
mu_data1 = []
for i in range(len(my_data)):
    data1.append(float(my_data[i][0]))
    mu_data1.append(float(my_data[i][1]))

################################
################################ pi = 2
################################
pihat = 0.5
piw = np.exp(pihat)
pim = np.exp(pihat)

res_ev2 = []
res_ev_n2 = []
for i in range(len(mu_plot)):
    
    phi1_n2 = np.zeros(D-1)
    phi2_n2 = np.zeros(D-1)
    for j in range(D-1):
        k = 0
        for k in range(5000):
            phi1_n_old = phi1_n2[j]
            phi2_n_old = phi2_n2[j]
            phi1_n2[j] = phi1_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
            phi2_n2[j] = phi2_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
    
    res_ev2.append(rescue(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew))
    res_ev_n2.append(rescue_num(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew,1-phi1_n2,1-phi2_n2))

my_data = genfromtxt('rescue_vary_mu_pi1_05_pi2_05.txt', delimiter=',')

data2 = []
mu_data2 = []
for i in range(len(my_data)):
    data2.append(float(my_data[i][0]))
    mu_data2.append(float(my_data[i][1]))

################################
################################ pi =0.5
################################
pihat = -0.5
piw = np.exp(pihat)
pim = np.exp(pihat)

res_ev3 = []
res_ev_n3 = []
for i in range(len(mu_plot)):
    
    phi1_n3 = np.zeros(D-1)
    phi2_n3 = np.zeros(D-1)
    for j in range(D-1):
        k = 0
        for k in range(5000):
            phi1_n_old = phi1_n3[j]
            phi2_n_old = phi2_n3[j]
            phi1_n3[j] = phi1_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
            phi2_n3[j] = phi2_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
    
    res_ev3.append(rescue(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew))
    res_ev_n3.append(rescue_num(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew,1-phi1_n3,1-phi2_n3))

my_data = genfromtxt('rescue_vary_mu_pi1_m05_pi2_m05.txt', delimiter=',')

data3 = []
mu_data3 = []
for i in range(len(my_data)):
    data3.append(float(my_data[i][0]))
    mu_data3.append(float(my_data[i][1]))

################################
################################ pi1 =2, pi2=0.5
################################
pi1hat = 0.5
pi2hat = -0.5
piw = np.exp(pi1hat)
pim = np.exp(pi2hat)

res_ev4 = []
res_ev_n4 = []
for i in range(len(mu_plot)):
    
    phi1_n4 = np.zeros(D-1)
    phi2_n4 = np.zeros(D-1)
    for j in range(D-1):
        k = 0
        for k in range(10000):
            phi1_n_old = phi1_n4[j]
            phi2_n_old = phi2_n4[j]
            phi1_n4[j] = phi1_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
            phi2_n4[j] = phi2_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
    
    res_ev4.append(rescue(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew))
    res_ev_n4.append(rescue_num(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew,1-phi1_n4,1-phi2_n4))

my_data = genfromtxt('rescue_vary_mu_pi1_05_pi2_m05.txt', delimiter=',')

data4 = []
mu_data4 = []
for i in range(len(my_data)):
    data4.append(float(my_data[i][0]))
    mu_data4.append(float(my_data[i][1]))


################################
################################ pi1 =0.5, pi2=2
################################
pi1hat = -0.5
pi2hat = 0.5
piw = np.exp(pi1hat)
pim = np.exp(pi2hat)

res_ev5 = []
res_ev_n5 = []
for i in range(len(mu_plot)):
    
    phi1_n5 = np.zeros(D-1)
    phi2_n5 = np.zeros(D-1)
    for j in range(D-1):
        k = 0
        for k in range(10000):
            phi1_n_old = phi1_n5[j]
            phi2_n_old = phi2_n5[j]
            phi1_n5[j] = phi1_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
            phi2_n5[j] = phi2_num((j+1)/D,piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
    
    res_ev5.append(rescue(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew))
    res_ev_n5.append(rescue_num(piw,pim,mu_plot[i],w_m,w_w,v_w,v_m,tau,D,u,Kold,Knew,1-phi1_n5,1-phi2_n5))

my_data = genfromtxt('rescue_vary_mu_pi1_m05_pi2_05.txt', delimiter=',')

data5 = []
mu_data5 = []
for i in range(len(my_data)):
    data5.append(float(my_data[i][0]))
    mu_data5.append(float(my_data[i][1]))


################################
################################ plot
################################
plt.semilogx(mu_plot,np.maximum(res_ev1,0),linewidth=2,linestyle='dashed',color='black')
plt.semilogx(mu_plot, res_ev_n1,linewidth=2,color = 'black')
plt.semilogx(mu_data1, data1,'o',color='black',markersize=10)

plt.plot(mu_plot,np.maximum(res_ev2,0),linewidth=2,linestyle='dashed',color='blue')
plt.plot(mu_plot,res_ev_n2,linewidth=2,color='blue')
plt.plot(mu_data2,data2,'b^',markersize=10)

plt.plot(mu_plot,np.maximum(res_ev3,0),linewidth=2,linestyle='dashed',color='green')
plt.plot(mu_plot,res_ev_n3,linewidth=2,color='green')
plt.plot(mu_data3,data3,'gv',markersize=10)

plt.semilogx(mu_plot,np.maximum(res_ev4,0),linewidth=2,linestyle='dashed',color='orange')
plt.plot(mu_plot,res_ev_n4,linewidth=2,color='orange')
plt.plot(mu_data4,data4,'s',color='orange',markersize=10) 

plt.semilogx(mu_plot,np.maximum(res_ev5,0),linewidth=2,linestyle='dashed',color='purple')
plt.plot(mu_plot,res_ev_n5,linewidth=2,color='purple')
plt.plot(mu_data5,data5,'D',color='purple',markersize=10) 

plt.xlim((0.001,1))
plt.ylim((0,1))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10) 
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5) 

plt.show()
