import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))


################################
################################ Data
################################

my_data = genfromtxt('vary_mu_rescue_origin_weak.txt', delimiter=',')

prob1 = []
prob2 = []
prob3 = []
mu_data = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1.append(float(my_data[i][0])/tot)
    prob2.append(float(my_data[i][1])/tot)
    prob3.append(float(my_data[i][2])/tot)
    mu_data.append(float(my_data[i][3]))

my_data = genfromtxt('vary_mu_rescue_origin_strong.txt', delimiter=',')

prob1s = []
prob2s = []
prob3s = []
mu_datas = []
for i in range(len(my_data)):
    tot = float(my_data[i][0])+float(my_data[i][1]) +float(my_data[i][2])
    prob1s.append(float(my_data[i][0])/tot)
    prob2s.append(float(my_data[i][1])/tot)
    prob3s.append(float(my_data[i][2])/tot)
    mu_datas.append(float(my_data[i][3]))


################################
################################ plot
################################
plt.semilogx(mu_data,prob1,'x',color='black',ms=10,mew=2)
plt.plot(mu_data,prob2,'+',color='black',ms=10,mew=2)
plt.plot(mu_data,prob3,'o',fillstyle='none',color='black',ms=10,mew=2)
plt.semilogx(mu_datas,prob1s,'x',color='y',ms=10,mew=2)
plt.plot(mu_datas,prob2s,'+',color='y',ms=10,mew=2)
plt.plot(mu_datas,prob3s,'o',fillstyle='none',color='y',ms=10,mew=2)

plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.xlim((10**(-3),1))
plt.show()

