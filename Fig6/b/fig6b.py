import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))


################################
################################ tau = 100
################################
my_data = genfromtxt('rescue_sgv_types_tau_100.txt', delimiter=',')

d1_sgv = []
d1_dn = []
d1_both = []
m1_plot = []
for i in range(len(my_data)):
    d1_sgv.append(float(my_data[i][0]))
    d1_dn.append(float(my_data[i][1]))
    d1_both.append(float(my_data[i][2]))
    m1_plot.append(float(my_data[i][3]))


################################
################################ tau = 50
################################
my_data = genfromtxt('rescue_sgv_types_tau_10.txt', delimiter=',')

d2_sgv = []
d2_dn = []
d2_both = []
m2_plot = []
for i in range(len(my_data)):
    d2_sgv.append(float(my_data[i][0]))
    d2_dn.append(float(my_data[i][1]))
    d2_both.append(float(my_data[i][2]))
    m2_plot.append(float(my_data[i][3]))


################################
################################ plot
################################
plt.loglog(m1_plot, d1_sgv,'x',fillstyle='none',color='black',markersize=10,mew=2)
plt.plot(m1_plot,d1_dn,'+',fillstyle='none',color='black',markersize = 10,mew=2)
plt.plot(m1_plot,d1_both,'o',fillstyle='none',color='black',markersize = 10,mew=2)
plt.semilogx(m2_plot, d2_sgv,'x',fillstyle='none',color='C9',markersize=10,mew=2)
plt.plot(m2_plot,d2_dn,'+',fillstyle='none',color='C9',markersize = 10,mew=2)
plt.plot(m2_plot,d2_both,'o',fillstyle='none',color='C9',markersize = 10,mew=2)

plt.xlim((0.001,1))
plt.ylim((0,1))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.show()

