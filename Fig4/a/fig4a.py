import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))

################################
################################ Theory
################################

def phi2(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = snew + snew * (1-f+pim*f)*(snew-sold)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (snew-sold)*pim*f)/(np.sqrt(nenner))
    return(result)

def phi1(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = sold + sold * (1-f+pim*f)*(sold-snew)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (sold-snew)*(1-f))/(np.sqrt(nenner))
    return(result)

def phi1_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp((1-m1)*(1+sold)*(p1-1) + m1*(1+snew)*(p2-1))
    return(result)

def phi2_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp(m2*(1+sold)*(p1-1) + (1-m2)*(1+snew)*(p2-1))
    return(result)

def pop2(f,piw,mu,v_w,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    result = min(Knew,mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu))))
    return(result)

def proba(f,piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew):
    Phi = 1-np.exp(-tfin*u*D*(phi1(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew)*f*Kold + phi2(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew)*(1-f)*pop2(f,piw,mu,v_w,Kold,Knew)))
    return(Phi)

def proba_n(f,piw,pim,mu,w_m,w_w,v_w,v_m,tfin,D,u,Kold,Knew,v1,v2):
    Phi = 1-np.exp(-tfin*u*D*(v1*f*Kold + v2*(1-f)*pop2(f,piw,mu,v_w,Kold,Knew)))
    return(Phi)


############### GLOBAL PARAMETER
w_m = 1.35
w_w = 1.5
v_w = 0.75
v_m = 1.02
mu = 0.06
f = np.arange(0.01,0.95,0.001)
Kold = 1000
Knew = 500
D = 10
tfin = 100
u = 1/(Knew*D)

############ bias
pihat = 0.
piw = np.exp(pihat)
pim = np.exp(pihat)

################################
################################ data
################################
my_data = genfromtxt('vary_f_mut_pi1_0_pi2_0_origin.txt', delimiter=',')

prob1 = []
prob2 = []
prob3 = []
freq = []
for i in range(len(my_data)):
    prob1.append(float(my_data[i][0]))
    prob2.append(float(my_data[i][1]))
    prob3.append(float(my_data[i][2]))
    freq.append(float(my_data[i][3]))

freq = np.asarray(freq)

f = np.arange(0.01,1.,0.01)
phi1_ev1 = []
phi2_ev1 = []
pop2_ev1 = []
for i in range(len(f)):
    phi1_ev1.append(np.maximum(phi1(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew),0))
    phi2_ev1.append(np.maximum(phi2(f[i],piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew),0))
    pop2_ev1.append(pop2(f[i],piw,mu,v_w,Kold,Knew))

ratio_plot1 = []
ratio_plot2 = []
ratio_plot3 = []
for i in range(len(f)):
    ratio_plot1.append((1-np.exp(-u*tfin*D*f[i]*Kold*phi1_ev1[i]))*np.exp(-u*tfin*D*(1-f[i])*pop2_ev1[i]*phi2_ev1[i]))
    ratio_plot2.append((1-np.exp(-u*tfin*D*(1-f[i])*phi2_ev1[i]*pop2_ev1[i]))*np.exp(-u*tfin*D*f[i]*Kold*phi1_ev1[i]))
    ratio_plot3.append((1-np.exp(-u*D*tfin*f[i]*Kold*phi1_ev1[i]))*(1-np.exp(-u*D*tfin*(1-f[i])*pop2_ev1[i]*phi2_ev1[i])))


################################
################################ plot
################################
plt.plot(f,ratio_plot1,linewidth=2,linestyle='solid',color='black')
plt.plot(f,ratio_plot2,linewidth=2,linestyle='dashed',color='black')
plt.plot(f,ratio_plot3,linewidth=2,linestyle='dotted',color='black')

plt.plot(freq/10,prob1,'x',color='black',markersize=10,mew=2)
plt.plot(freq/10,prob2,'+',color='black',markersize=10,mew=2)
plt.plot(freq/10,prob3,'o',fillstyle='none',color='black',markersize=10,mew=2)

plt.ylim((-0.05,0.4))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.show()
