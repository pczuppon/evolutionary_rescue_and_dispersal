#!/bin/bash

g++ -std=c++11 sim_vary_f_mut_origin.cpp `pkg-config --libs gsl`

for i in 1 2 3 4 5 6 7 8 9
do
    qsub script.sh "$i"
done
