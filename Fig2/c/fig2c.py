import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################

os.chdir(os.path.realpath(''))

################################
################################ Theory 
################################

def phi2(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = snew + snew * (1-f+pim*f)*(snew-sold)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (snew-sold)*pim*f)/(np.sqrt(nenner))
    return(result)

def phi1(f,piw,pim,mu,w_m,w_w,v_w,v_m,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    nenner = (1-f+pim*f)*((1-f)*(snew-sold+mu)**2 + pim*f*(snew-sold-mu)**2)
    result = sold + sold * (1-f+pim*f)*(sold-snew)/(np.sqrt(nenner)) + mu*(snew*(1-f) + sold*pim*f - (sold-snew)*(1-f))/(np.sqrt(nenner))
    return(result)

def phi1_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp((1-m1)*(1+sold)*(p1-1) + m1*(1+snew)*(p2-1))
    return(result)

def phi2_num(f,piw,pim,mu,w_m,w_w,v_w,v_m,p1,p2,Kold,Knew):
    m1 = mu*(1-f)/(1-f+piw*f)
    m2 = mu*piw*f/(1-f+piw*f)
    wt_stat_old = Kold
    wt_stat_new = mu*v_w*f*Kold/(1-f+piw*f-v_w*(1-f+piw*f*(1-mu)))
    wt_disp_old = (1-mu+m2)*wt_stat_old + m2 * (1-f)/f * min(Knew,wt_stat_new)
    wt_disp_new = m1*f/(1-f) *Kold + (1-mu+m1)*min(Knew,wt_stat_new)
    sold = Kold*w_m/(w_w*wt_disp_old) - 1
    if (wt_disp_new >= Knew/v_w):
        snew = Knew*v_m/(v_w*wt_disp_new)-1
    else:
        snew = v_m-1
    m1 = mu*(1-f)/(1-f+pim*f)
    m2 = mu*pim*f/(1-f+pim*f)
    result = np.exp(m2*(1+sold)*(p1-1) + (1-m2)*(1+snew)*(p2-1))
    return(result)

################################
################################ parameter definition
################################
f = 0.5
w_m = 1.45
w_w = 1.5
v_w = 0.75
v_m = 1.02
Kold = 1000
Knew = 500
mu = np.arange(0.0005,1.001,0.001)

################################
################################ pi = 1
################################
pihat = 0.
piw = np.exp(pihat)
pim = np.exp(pihat)

phi1_ev1 = []
for i in range(len(mu)):
    phi1_ev1.append(np.maximum(phi1(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,Kold,Knew),0))

my_data = genfromtxt('vary_mu_phi1_pi1_0_pi2_0.txt', delimiter=',')

data1 = []
mu_data1 = []
for i in range(len(my_data)):
    data1.append(float(my_data[i][0]))
    mu_data1.append(float(my_data[i][1]))

phi1_n1 = np.zeros(len(mu))
phi2_n1 = np.zeros(len(mu))
for i in range(len(mu)):
    print(i)
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n1[i]
        phi2_n_old = phi2_n1[i]
        phi1_n1[i] = phi1_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n1[i] = phi2_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

################################
################################ pi = 2
################################
pihat = 0.5
piw = np.exp(pihat)
pim = np.exp(pihat)

phi1_ev2 = []
for i in range(len(mu)):
    phi1_ev2.append(np.maximum(phi1(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,Kold,Knew),0))

my_data = genfromtxt('vary_mu_phi1_pi1_05_pi2_05.txt', delimiter=',')

data2 = []
mu_data2 = []
for i in range(len(my_data)):
    data2.append(float(my_data[i][0]))
    mu_data2.append(float(my_data[i][1]))

phi1_n2 = np.zeros(len(mu))
phi2_n2 = np.zeros(len(mu))
for i in range(len(mu)):
    print(i)
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n2[i]
        phi2_n_old = phi2_n2[i]
        phi1_n2[i] = phi1_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n2[i] = phi2_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

################################
################################ pi =05
################################
pihat = -0.5
piw = np.exp(pihat)
pim = np.exp(pihat)

phi1_ev3 = []

for i in range(len(mu)):
    phi1_ev3.append(np.maximum(phi1(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,Kold,Knew),0))

my_data = genfromtxt('vary_mu_phi1_pi1_m05_pi2_m05.txt', delimiter=',')

data3 = []
mu_data3 = []
for i in range(len(my_data)):
    data3.append(float(my_data[i][0]))
    mu_data3.append(float(my_data[i][1]))

phi1_n3 = np.zeros(len(mu))
phi2_n3 = np.zeros(len(mu))
for i in range(len(mu)):
    print(i)
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n3[i]
        phi2_n_old = phi2_n3[i]
        phi1_n3[i] = phi1_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n3[i] = phi2_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

################################
################################ pi1 = 2, pi2 =05
################################
pi1hat = 0.5
pi2hat = -0.5
piw = np.exp(pi1hat)
pim = np.exp(pi2hat)

my_data = genfromtxt('vary_mu_phi1_pi1_05_pi2_m05.txt', delimiter=',')

data4 = []
mu_data4 = []
for i in range(len(my_data)):
    data4.append(float(my_data[i][0]))
    mu_data4.append(float(my_data[i][1]))

phi1_ev4 = []

for i in range(len(mu)):
    phi1_ev4.append(np.maximum(phi1(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,Kold,Knew),0))

phi1_n4 = np.zeros(len(mu))
phi2_n4 = np.zeros(len(mu))
for i in range(len(mu)):
    print(i)
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n4[i]
        phi2_n_old = phi2_n4[i]
        phi1_n4[i] = phi1_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n4[i] = phi2_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

################################
################################ pi1 = 0.5, pi2 =2
################################
pi1hat = -0.5
pi2hat = 0.5
piw = np.exp(pi1hat)
pim = np.exp(pi2hat)

my_data = genfromtxt('vary_mu_phi1_pi1_m05_pi2_05.txt', delimiter=',')

data5 = []
mu_data5 = []
for i in range(len(my_data)):
    data5.append(float(my_data[i][0]))
    mu_data5.append(float(my_data[i][1]))

phi1_ev5 = []

for i in range(len(mu)):
    phi1_ev5.append(np.maximum(phi1(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,Kold,Knew),0))

phi1_n5 = np.zeros(len(mu))
phi2_n5 = np.zeros(len(mu))
for i in range(len(mu)):
    j = 0
    for j in range(5000):
        phi1_n_old = phi1_n5[i]
        phi2_n_old = phi2_n5[i]
        phi1_n5[i] = phi1_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)
        phi2_n5[i] = phi2_num(f,piw,pim,mu[i],w_m,w_w,v_w,v_m,phi1_n_old,phi2_n_old,Kold,Knew)

################################
################################ Plot
################################
#plt.loglog(mu,np.maximum(phi1_ev1,0),linewidth=2,linestyle='dashed',color='black')
plt.semilogx(mu,np.maximum(phi1_ev1,0),linewidth=2,linestyle='dashed',color='black')
plt.plot(mu_data1, data1,'o',color='black',markersize=10)
plt.plot(mu, 1-phi1_n1,linewidth=2,color = 'black')
 
plt.plot(mu,np.maximum(phi1_ev2,0),linewidth=2,linestyle='dashed',color='blue')
plt.plot(mu,1-phi1_n2,linewidth=2,color='blue')
plt.plot(mu_data2,data2,'b^',markersize=10)

plt.plot(mu,np.maximum(phi1_ev3,0),linewidth=2,linestyle='dashed',color='green')
plt.plot(mu,1-phi1_n3,linewidth=2,color='green')
plt.plot(mu_data3,data3,'gv',markersize=10) 

plt.plot(mu,np.maximum(phi1_ev4,0),linewidth=2,linestyle='dashed',color='orange')
plt.plot(mu,1-phi1_n4,linewidth=2,color='orange')
plt.plot(mu_data4,data4,'s',color='orange',markersize=10) 

plt.plot(mu,np.maximum(phi1_ev5,0),linewidth=2,linestyle='dashed',color='purple')
plt.plot(mu,1-phi1_n5,linewidth=2,color='purple')
plt.plot(mu_data5,data5,'D',color='purple',markersize=10) 

plt.ylim((0., 0.25))
plt.xlim((0.001,1))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)

plt.show()



