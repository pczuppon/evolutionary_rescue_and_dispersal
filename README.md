# Evolutionary_rescue_and_dispersal

Code that was used to generate the figures of the manuscript "The effect of habitat choice on evolutionary rescue in subdivided populations" available on bioRxiv (doi: 10.1101/738898)

